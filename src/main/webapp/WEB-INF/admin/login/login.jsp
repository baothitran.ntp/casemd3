<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <link rel="stylesheet" href="/assets/cssLogin/style.css">
</head>

<body>
<h2>abcaca STORE : Sign in/up Form</h2>
<div class="container" id="container">
  <div class="form-container sign-up-container">
    <form action="/login?action=create" method="post">
      <h1>Create Account</h1>

      <span>or use your email for registration</span>
      <c:if test="${requestScope.errors != null}">
        <div class="alert alert-danger">
          <ul>
            <c:forEach items="${errors}" var="e">
              <li>${e}</li>
            </c:forEach>
          </ul>
        </div>
      </c:if>
      <input type="text" name="userName" id="userName" placeholder="Name" />
      <input type="email" name="userEmail" id="userEmail" placeholder="Email" />
      <input type="password" name="password" id="password" placeholder="Password" />
      <button type="submit">Register</button>

    </form>
  </div>
  <div class="form-container sign-in-container">
    <form action="#">
      <h1>Sign in</h1>
      <span>or use your account</span>
      <input type="email" placeholder="Email" />
      <input type="password" placeholder="Password" />
      <a href="#">Forgot your password?</a>
      <button>Sign In</button>
    </form>
  </div>
  <div class="overlay-container">
    <div class="overlay">
      <div class="overlay-panel overlay-left">
        <h1>Welcome Back!</h1>
        <p>To keep connected with us please login with your personal info</p>
        <button class="ghost" id="signIn">Sign In</button>
      </div>
      <div class="overlay-panel overlay-right">
        <h1>Hello, Friend!</h1>
        <p>Enter your personal details and start journey with us</p>
        <button class="ghost" id="signUp">Sign Up</button>
      </div>
    </div>
  </div>
</div>
<script src="/assets/js/login.js"></script>
</body>

</html>