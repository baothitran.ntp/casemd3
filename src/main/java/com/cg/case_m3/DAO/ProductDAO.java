package com.cg.case_m3.DAO;

import com.cg.case_m3.context.DBContext;
import com.cg.case_m3.model.Category;
import com.cg.case_m3.model.Color;
import com.cg.case_m3.model.Product;
import com.cg.case_m3.service.ProductService;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProductDAO implements ProductService {
    Connection connection = null;
    PreparedStatement ps = null;
    ResultSet rs = null;


    public List<Product> getProduct() {
        List<Product> list = new ArrayList<>();
        String sql = "select c.category_name , p.product_id , p.product_name, p.quantity, p.product_price, p.product_describe,p.product_img from product p join category c on p.category_id = c.category_id;";
        try {
            connection = new DBContext().getConnection();
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Category category = new Category();
                category.setCatrgoryName(rs.getString(1));
                list.add(new Product(category, rs.getInt(2), rs.getString(3), rs.getInt(4), rs.getDouble(5), rs.getString(6), rs.getString(7)));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }


//    public List<Product> getProduct() {
//        List<Product> product = new ArrayList<>();
//        try {
//            connection = (Connection) new DBContext().getConnection();
//            ps = (PreparedStatement) ps.executeQuery();
//            PreparedStatement preparedStatement = connection.prepareStatement(
//                    "select p.product_id , p.product_name, p.quantity, p.product_price,c.category_name, p.product_describe,p.product_img from product p join category c on p.category_id = c.category_id;");
//
//            System.out.println("Function findAll " + preparedStatement);
//
//            // Thuc thi cau lenh: executeQuery - select, executeUpdate - them/xoa/sua
//            ResultSet rs = preparedStatement.executeQuery();
//            // rs.next(): đọc qua từng dòng
//            while (rs.next()) {
//                // getInt, getString : lấy giá trị theo tên cột hoặc chỉ số cột (bat dau tu 1)
//                Product p = getCustomerFromRs2(rs);
//                product.add(p);
//            }
//
//        } catch (Exception sqlException) {
//            printSQLException((SQLException) sqlException);
//        }
//        return product;
//    }

    private Product getCustomerFromRs2(ResultSet rs) throws SQLException {
        int productId = rs.getInt("productId");
        String productName = rs.getString("productName");
        int quantity = rs.getInt("quantity");
        double productPrice = rs.getDouble("productPrice");
        int categoryId = rs.getInt("categoryId");
        String productDescribe = rs.getString("productDescribe");
        String productImg = rs.getString("productImg");


        String categoryName = rs.getString("categoryName");



        Category category = new Category(categoryId, categoryName);
        Product product = new Product(productId, productName, quantity, productPrice, categoryId, productDescribe, productImg);
        product.setCategory(category);
        return  product;
    }



    public void insertProduct(Product product) {
        String sql = "insert into product (product_name,quantity,product_price,category_id,brand_id,memory_id,color_id,product_describe,product_img) values(?,?,?,?,?,?,?,?,?);";
        try {
            connection = new DBContext().getConnection();
            ps = connection.prepareStatement(sql);
            ps.setString(1,product.getProductName());
            ps.setInt(2,product.getQuantity());
            ps.setDouble(3,product.getProductPrice());
            ps.setInt(4,product.getCategoryId());
            ps.setInt(5,product.getBrandId());
            ps.setInt(6,product.getMemoryId());
            ps.setInt(7,product.getColorId());
            ps.setString(8, product.getProductDescribe());
            ps.setString(9, product.getProductImg());

            System.out.println("Function save " + ps);
            ps.executeUpdate();

            connection.close();
        } catch (SQLException sqlException) {
            printSQLException(sqlException);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void ProductDelete(String productId) {
        String sql = "delete from product where product_id=?;";
        try {
            connection = new DBContext().getConnection();
            ps = connection.prepareStatement(sql);
            ps.setString(1, productId);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void updateProduct(Product product) {
        String sql = "UPDATE `product` SET `product_name` = ?, `quantity` = ?, `product_price` = ?, `category_id` = ?, `brand_id` = ?, `memory_id` = ?, `color_id` = ?, `product_describe` = ?, `product_img` = ? WHERE (`product_id` = ?);";
        try {
            connection = new DBContext().getConnection();
            ps = connection.prepareStatement(sql);
            ps.setString(1,product.getProductName());
            ps.setInt(2,product.getQuantity());
            ps.setDouble(3,product.getProductPrice());
            ps.setInt(4,product.getCategoryId());
            ps.setInt(5,product.getBrandId());
            ps.setInt(6,product.getMemoryId());
            ps.setInt(7,product.getColorId());
            ps.setString(8, product.getProductDescribe());
            ps.setString(9, product.getProductImg());
            ps.setInt(10,product.getProductId());
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Color> getColor() {
        List<Color> list = new ArrayList<>();
        String sql = "select * from product_color;";
        try {
            connection = new DBContext().getConnection();
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Color(rs.getInt(1), rs.getString(2)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Category> getCategory() {
        List<Category> list = new ArrayList<>();
        String sql = "select * from category;";
        try {
            connection = new DBContext().getConnection();
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Category(rs.getInt(1), rs.getString(2)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public Category getCategoryByName(String name) {
        String sql = "select * from category where category_name = ?;";
        try {
            connection = new DBContext().getConnection();
            ps = connection.prepareStatement(sql);
            ps.setString(1, name);
            rs = ps.executeQuery();
            while (rs.next()) {
                return new Category(rs.getInt(1), rs.getString(2));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public List<Product> getTop10Product() {
        List<Product> list = new ArrayList<>();
        String sql = "SELECT p.product_id , p.product_name, p.quantity, p.product_price, p.product_describe,p.product_img FROM product p ORDER BY p.product_id DESC LIMIT 10;";
        try {
            connection = new DBContext().getConnection();
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getDouble(4), rs.getString(5), rs.getString(6)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Product> getTrendProduct() {
        List<Product> list = new ArrayList<>();
        String sql = "SELECT p.product_id , p.product_name, p.quantity, p.product_price, p.product_describe, p.product_img FROM product p \n" +
                "join (select b.product_id, count(b.product_id) as countProduct from bill_detail b group by b.product_id order by countProduct DESC LIMIT 5) as bd on p.product_id = bd.product_id;";
        try {
            connection = new DBContext().getConnection();
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getDouble(4), rs.getString(5), rs.getString(6)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Product> getProductByPriceAsc() {
        List<Product> list = new ArrayList<>();
        String sql = "select c.category_name , p.product_id , p.product_name, p.quantity, p.product_price, p.product_describe,p.product_img from product p join category c on p.category_id = c.category_id;";
        try {
            connection = new DBContext().getConnection();
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Category category = new Category();
                category.setCatrgoryName(rs.getString(1));
                list.add(new Product(category, rs.getInt(2), rs.getString(3), rs.getInt(4), rs.getDouble(5), rs.getString(6), rs.getString(7)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Product> getProductByPriceDesc() {
        List<Product> list = new ArrayList<>();
        String sql = "select c.category_name , p.product_id , p.product_name, p.quantity, p.product_price, p.product_describe,p.product_img from product p join category c on p.category_id = c.category_id;";
        try {
            connection = new DBContext().getConnection();
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Category category = new Category();
                category.setCatrgoryName(rs.getString(1));
                list.add(new Product(category, rs.getInt(2), rs.getString(3), rs.getInt(4), rs.getDouble(5), rs.getString(6), rs.getString(7)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Product> getProductByName() {
        List<Product> list = new ArrayList<>();
        String sql = "select c.category_name , p.product_id , p.product_name, p.quantity, p.product_price, p.product_describe,p.product_img from product p join category c on p.category_id = c.category_id;";
        try {
            connection = new DBContext().getConnection();
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Category category = new Category();
                category.setCatrgoryName(rs.getString(1));
                list.add(new Product(category, rs.getInt(2), rs.getString(3), rs.getInt(4), rs.getDouble(5), rs.getString(6), rs.getString(7)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Product> getListByPage(List<Product> list, int start, int end) {
        ArrayList<Product> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;
    }

    public List<Product> getProductByCategory(int categoryId) {
        List<Product> list = new ArrayList<>();
        String sql = "select c.category_name , p.product_id , p.product_name, p.quantity, p.product_price, p.product_describe, p.product_img from product p join category c on p.category_id = c.category_id WHERE p.category_id=?;";
        try {
            connection = new DBContext().getConnection();
            ps = connection.prepareStatement(sql);
            ps.setInt(1, categoryId);
            rs = ps.executeQuery();
            while (rs.next()) {
                Category category = new Category();
                category.setCatrgoryName(rs.getString(1));
                list.add(new Product(category, rs.getInt(2), rs.getString(3), rs.getInt(4), rs.getDouble(5), rs.getString(6), rs.getString(7)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }


    public Product getProductByID(String productId) {
        List<Product> list = new ArrayList<>();
        String sql = "select c.category_id, c.category_name , p.product_id , p.product_name,p.quantity, p.product_price, p.product_describe, p.product_img from product p join category c on p.category_id = c.category_id WHERE p.product_id=?;";
        try {
            connection = new DBContext().getConnection();
            ps = connection.prepareStatement(sql);
            ps.setString(1, productId);
            rs = ps.executeQuery();
            while (rs.next()) {
                Category category = new Category(rs.getInt(1), rs.getString(2));
                return (new Product(category, rs.getInt(3), rs.getString(4), rs.getInt(5), rs.getDouble(6), rs.getString(7), rs.getString(8)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public List<Color> getColorByID(String productId) {
        List<Color> list = new ArrayList<>();
        String sql = "select * from product_color where color_id=?;";
        try {
            connection = new DBContext().getConnection();
            ps = connection.prepareStatement(sql);
            ps.setString(1, productId);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Color(rs.getInt(1), rs.getString(2)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public int CountProduct() {
        int count = 0;
        String sql = "SELECT COUNT(*) as 'count' FROM product;";
        try {
            connection = new DBContext().getConnection();
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                count = rs.getInt(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }

    public int CountUser() {
        int count = 0;
        String sql = "SELECT COUNT(*) as 'count' FROM user where isAdmin = 'False' or isAdmin = 'FALSE';";
        try {
            connection = new DBContext().getConnection();
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                count = rs.getInt(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }

    public int CountBill() {
        int count = 0;
        String sql = "SELECT COUNT(*) as 'count' FROM bill;";
        try {
            connection = new DBContext().getConnection();
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                count = rs.getInt(1);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return count;
    }

    public int CountProductLow() {
        int count = 0;
        String sql = "SELECT COUNT(*) as 'count' FROM product where quantity < 10;";
        try {
            connection = new DBContext().getConnection();
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                count = rs.getInt(1);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return count;
    }

    public List<Product> getProductLow() {
        List<Product> list = new ArrayList<>();
        String sql = "SELECT * FROM product where quantity < 10;";
        try {
            connection = new DBContext().getConnection();
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Category category = new Category();
                category.setCatrgoryName(rs.getString(1));
                list.add(new Product(category, rs.getInt(2), rs.getString(3), rs.getInt(4), rs.getDouble(5), rs.getString(6), rs.getString(7)));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Product> SearchAll(String text) {
        List<Product> list = new ArrayList<>();
        String sql = "SELECT DISTINCT c.category_name , p.product_id , p.product_name, p.product_price, p.product_describe, p.quantity,p.img \n"
                + "FROM product p join category c on c.category_id = p.category_id join product_color ps on p.product_id = ps.product_id\n"
                + "WHERE p.product_name LIKE ? OR  p.product_price LIKE ? OR ps.color LIKE ? OR c.category_name LIKE ?;";

        try {
            connection = new DBContext().getConnection();
            ps = connection.prepareStatement(sql);
            ps.setString(1, "%" + text + "%");
            ps.setString(2, "%" + text + "%");
            ps.setString(3, text);
            ps.setString(4, "_%" + text + "%_");
            rs = ps.executeQuery();
            while (rs.next()) {
                Category category = new Category();
                category.setCatrgoryName(rs.getString(1));
                list.add(new Product(category, rs.getInt(2), rs.getString(3), rs.getInt(4), rs.getDouble(5), rs.getString(6), rs.getString(7)));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return list;
    }


    public void printSQLException(SQLException ex) {
        for (Throwable e : ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }


    public Product findById(int id) {
        return null;
    }

    public void update(int id, Product customer) {
    }

    public void save(Product product) {
    }

    public List<Product> findAll2() {
        return null;
    }

    public List<Product> findAll() {
        return null;
    }

    public void remove(int id) {
    }
}
