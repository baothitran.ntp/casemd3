package com.cg.case_m3.controller;

import com.cg.case_m3.model.User;
import com.cg.case_m3.utils.ValidateUtils;
import com.cg.case_m3.service.IUserService;
import com.cg.case_m3.service.UserService;


import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;



@WebServlet(name = "UserServlet", value = "/login")
public class UserServlet extends HttpServlet {

    private IUserService userService;

    @Override
    public void init() throws ServletException {
        userService = new UserService();


    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getParameter("action");
        if (action == null) {
            action = "";
        }
        switch (action) {
            case "create":
                showFormLogin(req,resp);
                break;
            default:
                showFormLogin(req,resp);
        }
    }

    private void showFormLogin(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/admin/login/login.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getParameter("action");
        if (action == null) {
            action = "";
        }
        switch (action) {
            case "create":
                createUser(req,resp);
                break;
            default:
                showFormLogin(req,resp);
        }
    }

    private void createUser(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<String> errors = new ArrayList<>();
        User user = new User();
        String userName = req.getParameter("userName");
        String userEmail = req.getParameter("userEmail");
        String userPass = req.getParameter("password");

        if (!ValidateUtils.isUserName(userName)) {
            errors.add("Username không phù hợp");
        }

        if (!ValidateUtils.isEmail(userEmail)) {
            errors.add("Email không phù hợp");
        }
        if (!ValidateUtils.isPassWord(userPass)) {
            errors.add("Password không phù hợp");
        }
        if (errors.size() == 0) {
            user.setUserName(userName);
            user.setUserEmail(userEmail);
            user.setUserPass(userPass);
            userService.create(user);
            req.setAttribute("message", "Tạo tài khoản thành công");
        } else {
            req.setAttribute("errors", errors);
        }
        req.getRequestDispatcher("/WEB-INF/admin/login/login.jsp").forward(req,resp);
    }

}
