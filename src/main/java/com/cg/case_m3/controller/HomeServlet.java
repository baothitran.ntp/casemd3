package com.cg.case_m3.controller;

import com.cg.case_m3.DAO.ProductDAO;
import com.cg.case_m3.model.Product;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "homeServlet", urlPatterns = "/home")
public class HomeServlet extends HttpServlet {
    @Override
    public void init() throws ServletException {
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");
        ProductDAO productDAO = new ProductDAO();
        List<Product> product = productDAO.getTop10Product();
        List<Product> product1 = productDAO.getTrendProduct();
        request.setAttribute("top10", product);
        request.setAttribute("topTrend", product1);
        request.getRequestDispatcher("/WEB-INF/admin/customer/index.jsp").forward(request, response);

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public void destroy() {
    }
}
