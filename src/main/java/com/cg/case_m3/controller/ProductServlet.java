package com.cg.case_m3.controller;

import com.cg.case_m3.DAO.ProductDAO;
import com.cg.case_m3.model.Category;
import com.cg.case_m3.model.Color;
import com.cg.case_m3.model.Product;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@WebServlet(name = "ProductServlet", urlPatterns = "/product")
public class ProductServlet extends HttpServlet {
    private ProductDAO productDAO;

    @Override
    public void init() throws ServletException {
        productDAO = new ProductDAO();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        String action = req.getParameter("action");
        if(action == null) {
            action = "";
        }
        switch (action) {
            case "productDetail":
                showProductDetail(req,resp);
                break;
            default:
                showList(req,resp);
        }
    }

    private void showProductDetail(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String productId = req.getParameter("product_id");
        List<Color> color = productDAO.getColorByID(productId);
        Product product = productDAO.getProductByID(productId);
        List<Integer> numberOfProduct = Arrays.asList(1, 2, 3, 4);
        if (product == null) {
            req.getRequestDispatcher("/404.jsp").forward(req, resp);
        }
        int categoryId = product.getCategory().getCategoryId();
        List<Product> productByCategory = productDAO.getProductByCategory(categoryId);

        req.setAttribute("ProductData", product);
        req.setAttribute("ColorData", color);
        req.setAttribute("ProductByCategory", productByCategory);
        req.setAttribute("NumberOfProduct", numberOfProduct);
        req.getRequestDispatcher("/WEB-INF/admin/customer/productDetail.jsp").forward(req, resp);
    }

    private void showList(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Product> productList = productDAO.getProduct();
        List<Category> category = productDAO.getCategory();
        int page, numperpage = 9;
        int size = productList.size();
        int num = (size % 9 == 0 ? (size / 9) : ((size / 9)) + 1);//so trang
        String xpage = req.getParameter("page");
        if (xpage == null) {
            page = 1;
        } else {
            page = Integer.parseInt(xpage);
        }
        int start, end;
        start = (page - 1) * numperpage;
        end = Math.min(page * numperpage, size);
        List<Product> product = productDAO.getListByPage(productList, start, end);
        req.setAttribute("page", page);
        req.setAttribute("num", num);
        req.setAttribute("CategoryData", category);
        req.setAttribute("ProductData", product);
        req.getRequestDispatcher("/WEB-INF/admin/customer/product.jsp").forward(req, resp);
    }

    private void showlistByCategory(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String catgoryId = req.getParameter("catgoryId");
        int catgoryId1 = Integer.parseInt(catgoryId);
        List<Product> productList = productDAO.getProductByCategory(catgoryId1);
        List<Category> category = productDAO.getCategory();
        int page, numperpage = 9;
        int size = productList.size();
        int num = (size % 9 == 0 ? (size / 9) : ((size / 9)) + 1);//so trang
        String xpage = req.getParameter("page");
        if (xpage == null) {
            page = 1;
        } else {
            page = Integer.parseInt(xpage);
        }
        int start, end;
        start = (page - 1) * numperpage;
        end = Math.min(page * numperpage, size);
        List<Product> product = productDAO.getListByPage(productList, start, end);
        req.setAttribute("page", page);
        req.setAttribute("num", num);
        req.setAttribute("CategoryData", category);
        req.setAttribute("ProductData", product);
        req.getRequestDispatcher("/WEB-INF/admin/customer/product.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        String action = req.getParameter("action");
        if (action == null) {
            action = "";
        }
        switch (action) {
            case "search":
                searchProduct(req,resp);
                break;
            default:
        }
    }


    private void searchProduct(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String text = req.getParameter("text");
        List<Product> productList = productDAO.SearchAll(text);
        List<Category> category = productDAO.getCategory();
        int page, numperpage = 9;
        int size = productList.size();
        int num = (size % 9 == 9 ? (size / 9) : ((size / 9)) + 1);//so trang
        String xpage = req.getParameter("page");
        if (xpage == null) {
            page = 1;
        } else {
            page = Integer.parseInt(xpage);
        }
        int start, end;
        start = (page - 1) * numperpage;
        end = Math.min(page * numperpage, size);
        List<Product> product = productDAO.getListByPage(productList, start, end);
        req.setAttribute("page", page);
        req.setAttribute("num", num);
        req.setAttribute("CategoryData", category);
        req.setAttribute("ProductData", product);
        req.getRequestDispatcher("/WEB-INF/admin/customer/product.jsp").forward(req, resp);
    }


}
