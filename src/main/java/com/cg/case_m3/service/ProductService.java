package com.cg.case_m3.service;

import com.cg.case_m3.model.Product;

import java.util.ArrayList;
import java.util.List;

public interface ProductService {
    List<Product> findAll();
    List<Product> findAll2();
    void save(Product customer);
    Product findById(int id);
    void update(int id, Product customer);
    void remove(int id);

}
