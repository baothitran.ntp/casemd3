package com.cg.case_m3.service;

import com.cg.case_m3.DAO.ProductDAO;
import com.cg.case_m3.model.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductServiceImpl implements ProductService{
    private ProductDAO productDAO;
    public ProductServiceImpl() {
        productDAO = new ProductDAO();
    }
    @Override
    public List<Product> findAll() {
        return productDAO.findAll();
    }

    @Override
    public List<Product> findAll2() {
        return productDAO.findAll2();
    }

    @Override
    public void save(Product product) {
        productDAO.save(product);
    }

    @Override
    public Product findById(int id) {
        return productDAO.findById(id);
    }

    @Override
    public void update(int id, Product customer) {
        productDAO.update(id,customer);
    }

    @Override
    public void remove(int id) {
        productDAO.remove(id);
    }
}