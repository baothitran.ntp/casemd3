package com.cg.case_m3.service;

import com.cg.case_m3.context.DBContext;
import com.cg.case_m3.model.User;

import javax.servlet.ServletException;
import java.sql.Connection;
import java.sql.PreparedStatement;

public class UserService extends DBContext implements IUserService{

    @Override
    public void create(User user) {
        try {
           Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO `casestudy_module3`.`user` (`user_name`, `user_email`, `user_pass`, `is_admin`) VALUES (?, ?, ?, ?);");
            preparedStatement.setString(1, user.getUserName());
            preparedStatement.setString(2, user.getUserEmail());
            preparedStatement.setString(3, user.getUserPass());
            preparedStatement.setBoolean(4,false);
            preparedStatement.executeUpdate();
            System.out.println("Function create: "+ preparedStatement);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}