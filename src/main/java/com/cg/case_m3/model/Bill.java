package com.cg.case_m3.model;

import java.util.Date;

public class Bill {
    private int billId;
    private int userId;
    private double total;
    private String payment;
    private String address;
    private Date createAt;


    public Bill() {
    }

    public Bill(int billId, int userId, double total, String payment, String address, Date createAt) {
        this.billId = billId;
        this.userId = userId;
        this.total = total;
        this.payment = payment;
        this.address = address;
        this.createAt = createAt;
    }

    public int getBillId() {
        return billId;
    }

    public void setBillId(int billId) {
        this.billId = billId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }
}
