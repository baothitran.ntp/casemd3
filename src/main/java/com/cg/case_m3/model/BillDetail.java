package com.cg.case_m3.model;

public class BillDetail {
    private int detailId;
    private int billId;
    private int productId;
    private int quantity;
    private String screenSize;
    private String color;
    private double productPrice;

    public BillDetail(int detailId, int billId, int productId, int quantity, String screenSize, String color, double productPrice) {
        this.detailId = detailId;
        this.billId = billId;
        this.productId = productId;
        this.quantity = quantity;
        this.screenSize = screenSize;
        this.color = color;
        this.productPrice = productPrice;
    }

    public int getDetailId() {
        return detailId;
    }

    public void setDetailId(int detailId) {
        this.detailId = detailId;
    }

    public int getBillId() {
        return billId;
    }

    public void setBillId(int billId) {
        this.billId = billId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getScreenSize() {
        return screenSize;
    }

    public void setScreenSize(String screenSize) {
        this.screenSize = screenSize;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }
}
