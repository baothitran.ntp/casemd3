package com.cg.case_m3.model;

import java.util.ArrayList;
import java.util.List;

public class Product {
    private int productId;
    private String productName;
    private int quantity;
    private double productPrice;
    private int categoryId;
    private int brandId;
    private int memoryId;
    private int colorId;
    private String productDescribe;
    private String productImg;

    private Brand brand;
    private Category category;
    private Color color;
    private Memory memory;
    private boolean delete;


    public Product() {
    }

    public Product(int productId, String productName, int quantity, double productPrice, int categoryId, int brandId, int memoryId, int colorId, String productDescribe, String productImg, Brand brand, Category category, Color color, Memory memory) {
        this.productId = productId;
        this.productName = productName;
        this.quantity = quantity;
        this.productPrice = productPrice;
        this.categoryId = categoryId;
        this.brandId = brandId;
        this.memoryId = memoryId;
        this.colorId = colorId;
        this.productDescribe = productDescribe;
        this.productImg = productImg;
        this.brand = brand;
        this.category = category;
        this.color = color;
        this.memory = memory;
    }

    public Product(int productId, String productName, int quantity, double productPrice, int categoryId, int brandId, int memoryId, int colorId, String productDescribe, String productImg, Brand brand, Category category, Color color, Memory memory, boolean delete) {
        this.productId = productId;
        this.productName = productName;
        this.quantity = quantity;
        this.productPrice = productPrice;
        this.categoryId = categoryId;
        this.brandId = brandId;
        this.memoryId = memoryId;
        this.colorId = colorId;
        this.productDescribe = productDescribe;
        this.productImg = productImg;
        this.brand = brand;
        this.category = category;
        this.color = color;
        this.memory = memory;
        this.delete = delete;
    }

    public Product(int productId, String productName, int quantity, double productPrice, int categoryId, String productDescribe, String productImg) {
        this.productId = productId;
        this.productName = productName;
        this.quantity = quantity;
        this.productPrice = productPrice;
        this.categoryId = categoryId;
        this.productDescribe = productDescribe;
        this.productImg = productImg;
    }

    public Product(int productId, String productName, int quantity, double productPrice, String productDescribe, String productImg) {
        this.productId = productId;
        this.productName = productName;
        this.quantity = quantity;
        this.productPrice = productPrice;
        this.productDescribe = productDescribe;
        this.productImg = productImg;
    }

    public Product(Category category, int productId, String productName, int quantity, double productPrice, String productDescribe, String productImg) {
        this.category = category;
        this.productId = productId;
        this.productName = productName;
        this.quantity = quantity;
        this.productPrice = productPrice;
        this.productDescribe = productDescribe;
        this.productImg = productImg;
    }

    public boolean isDelete() {
        return delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public int getMemoryId() {
        return memoryId;
    }

    public void setMemoryId(int memoryId) {
        this.memoryId = memoryId;
    }

    public int getColorId() {
        return colorId;
    }

    public void setColorId(int colorId) {
        this.colorId = colorId;
    }

    public String getProductDescribe() {
        return productDescribe;
    }

    public void setProductDescribe(String productDescribe) {
        this.productDescribe = productDescribe;
    }

    public String getProductImg() {
        return productImg;
    }

    public void setProductImg(String productImg) {
        this.productImg = productImg;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Memory getMemory() {
        return memory;
    }

    public void setMemory(Memory memory) {
        this.memory = memory;
    }

}
