package com.cg.case_m3.model;

public class Category {
    private int categoryId;
    private String catrgoryName;

    public Category(int categoryId, String catrgoryName) {
        this.categoryId = categoryId;
        this.catrgoryName = catrgoryName;
    }

    public Category() {

    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCatrgoryName() {
        return catrgoryName;
    }

    public void setCatrgoryName(String catrgoryName) {
        this.catrgoryName = catrgoryName;
    }
}
